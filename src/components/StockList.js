import React, { useEffect, useState } from 'react';
import { ScaleLoader } from 'react-spinners';
import Stockcard from './StockCard';

function StockContainer({ stockData, fetchStocks }) {
  const [loading, setLoading] = useState(true);
  const [stocks, setStocks] = useState([]);

  useEffect(() => {
    fetchStocks();
  }, [fetchStocks]);

  fetchStocks = () => {

    fetch('https://financialmodelingprep.com/api/v3/actives?apikey=21da0a2595edbe0cef9cb1505b31a8eb')
      .then(response => {
        return response.json()
      })
      .then(data => {
        setStocks(data)
        setLoading(false)
      }).catch(err => {
        console(err)
      })
  }

  return loading ? (
    <h2 className="text-center pt-5">
      <ScaleLoader size={16} color="green" />
    </h2>
  ) : (
    <div className="mt-5 d-flex flex-wrap justify-content-center">
      {stocks.map(stockInfo => (
        <Stockcard
          key={stockInfo.ticker}
          stock={stockInfo}
        />
      ))}
    </div>
  );
}

export default StockContainer;

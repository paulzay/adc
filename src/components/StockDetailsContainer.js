import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { ScaleLoader } from 'react-spinners';
import StockDetail from './StockDetails';

export default function StockDetailsContainer() {
  const [loading, setLoading] = useState(true);
  const [stockItem, setStockItem] = useState([])
  const { ticker } = useParams();

  useEffect(() => {
    fetch(`https://financialmodelingprep.com/api/v3/profile/${ticker}?apikey=21da0a2595edbe0cef9cb1505b31a8eb`)
      .then(response => {
        return response.json()
      })
      .then(data => {
        setStockItem(data)
        console.log(stockItem)
        setLoading(false)
      }).catch(err => {
        console(err)
        setLoading(false)
      })
  }, [stockItem, ticker]);

  return loading ? (
    <h2 className="text-center pt-5">
      <ScaleLoader size={16} color="white" />
    </h2>
  ) : stockItem === undefined ? (
    <h2 className="text-center pt-5 pb-5">
      Stock Item Not Found!
    </h2>
  ) : (
    <StockDetail stockItem={stockItem} />
  );
}

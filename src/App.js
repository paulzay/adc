import StockList from './components/StockList';
import {
  Routes,
  Route,
  BrowserRouter as Router,
} from 'react-router-dom';
import Nav from './components/Nav';
import StockDetailsContainer from './components/StockDetailsContainer';

function App() {
  return (
    <div>
      <Router>
        <Nav />
        <Routes>
          <Route exact path="/" element={<StockList />} />
          <Route exact path="/stock/:ticker" element={<StockDetailsContainer />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
